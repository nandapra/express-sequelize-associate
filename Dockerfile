FROM keymetrics/pm2:10-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache \
  bash \
  git \
  curl \
  openssh

MAINTAINER Nanda Cahyangtia Pratama

RUN mkdir -p /usr/src/softdev
WORKDIR /usr/src/softdev

COPY package*.json ./
RUN npm cache clean --force
RUN npm install
COPY . .

EXPOSE 4040
EXPOSE 5432

CMD [ "pm2-runtime", "start", "pm2.json", "--env", "production" ]