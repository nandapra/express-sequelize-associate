const model = require('../models')
const company = model.Company

class CompanyController {
    constructor(){}

    async list(req, res){
        try{
            const companys = await company.findAll(
                {
                    order: [['id', 'ASC']],
                    include: ['as_employees', 'as_companyTools']
                }
            )
            res.status(200).send({
                message: 'Successed',
                data: companys
            })
        }catch(err){
            res.status(422).send({
                message: 'Failed',
                data: err.message
            })
        }
    }

    async create(req, res){
        try{
            await company.create({
                name: req.body.name,
                address: req.body.address
            })
            res.status(200).send({
                message: 'Successed',
                data: company
            })
        }catch (err){
            res.status(422).send({
                message: 'Failed',
                data: err.message
            })
        }
    }
}

module.exports = CompanyController