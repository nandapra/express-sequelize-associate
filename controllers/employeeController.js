const model = require('../models')
const bcrypt = require('bcrypt')
const employee = model.Employee
const tool = model.Tool
const driver = model.Driver

module.exports = {
    async list(req, res){
        console.log('req', req)
        try{
            const employees = await employee.findAll(
                {
                    order: [['id', 'ASC']],
                    attributes: ['id', 'name', 'phone', 'address', 'LOS'],
                    include: ['as_companies', 
                        {
                            model: tool,
                            as: 'as_tool',
                            attributes: ['id', 'name', 'category'],
                            through: {
                                attributes: []
                            }
                        },{
                            model: driver,
                            as: 'as_driver',
                            attributes: ['id', 'name', 'location'],
                            through: {
                                attributes: []
                            }
                        }
                    ]
                }
            )
            res.status(200).send({
                message: 'Successed',
                data: employees
            })
        }catch(err){
            res.status(422).send({
                message: 'Failed',
                data: err.message
            })
        }
    },

    async create(req, res){
        try{
            const password = req.body.password
            const saltRounds = 10
            const salt = bcrypt.genSaltSync(saltRounds)
            const passHash = bcrypt.hashSync(password, salt)
            await employee.create({
                name: req.body.name,
                phone: req.body.phone,
                address: req.body.address,
                LOS: req.body.los,
                companyId: req.body.companyId,
                password: passHash
            })
            res.status(200).send({
                message: 'Successed',
                data: employee
            })
        }catch(err){
            res.status(422).send({
                message: 'Failed',
                data: err.message
            })
        }
    }
}