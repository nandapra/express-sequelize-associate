const model = require('../models')
const bcrypt = require('bcrypt')
const JWT = require('jsonwebtoken')
const eLogin = model.Employee

module.exports = {
    async login(req, res){
        try{
            const phone = req.body.phone
            const password = req.body.password
            const user = await eLogin.findOne({
                where: {
                    phone: phone
                }
            })
            userSecure = {
                name: user.name,
                phone: user.phone,
                companyId: user.companyId
            }
            if(user === null){
                res.status(400).send({
                    message: "your phone is wrong",
                })
            }
            const pwd = user.password
            const comparePWD = bcrypt.compareSync(password, pwd)
            if (!comparePWD){
                res.status(422).send({
                    message: 'Password failed, please try again!!!'
                })
            }else{
                const token = JWT.sign(
                    {userSecure},
                    process.env.SECRETKEY
                )
                res.status(200).send({
                    message: 'Successed Login',
                    token
                })
            }
        }catch (err){
            res.status(422).send({
                message: err.message
            })
        }
    }
}