const model = require('../models')
const tool = model.Tool

module.exports = {
    async list(req, res){
        try{
            const tools = await tool.findAll(
                {
                    order: [['id', 'ASC']],
                    include: ['as_employeeTool', 'as_toolCompany']
                }
            )
            res.status(200).send({
                message: 'Successed',
                data: tools
            })
        }catch(err){
            res.status(422).send({
                message: 'Failed',
                data: err.message
            })
        }
    }
}