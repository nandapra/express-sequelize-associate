const passport = require('passport')
const passportJWT = require('passport-jwt')

const JWTStategy = passportJWT.Strategy
const ExtractJWT = passportJWT.ExtractJwt 

passport.use(new JWTStategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRETKEY
    },
    function(jwtPayload, done){
        try{
            return done(null, jwtPayload.userSecure)
        }catch(err){
            return done(error)
        }
    }
))