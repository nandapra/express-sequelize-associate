'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'Employees',
      'length of stay',
      {
        type: Sequelize.INTEGER
      })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Employees', 'length of stay');
  }
};
