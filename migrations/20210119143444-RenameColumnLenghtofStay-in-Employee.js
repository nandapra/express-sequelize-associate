'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn(
      'Employees',
      'length of stay',
      'LOS'
    )
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn(
      'Employees',
      'LOS',
      'length of stay'
    )
  }
};
