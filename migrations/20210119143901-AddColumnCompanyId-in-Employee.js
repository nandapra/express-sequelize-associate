'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'Employees',
      'companyId',
      {
        type: Sequelize.INTEGER,
        reference: 
        {
          model: 'Employees',
          key: 'id'
        }
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Employees', 'companyId');
  }
};
