'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn(
      'Pivots',
      'companyId',
      'employeeId'
    )
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn(
      'Pivots',
      'employeeId',
      'companyId'
    )
  }
};
