'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'Tools',
      'companyId',
      {
        type: Sequelize.INTEGER,
        reference: 
        {
          model: 'Tool',
          key: 'id'
        }
      }
    ) 
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Tools', 'companyId');
  }
};
