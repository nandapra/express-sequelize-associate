'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'Pivots',
      'driverId',
      {
        type: Sequelize.INTEGER,
        reference: 
        {
          model: 'Pivot',
          key: 'id'
        }
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Pivots', 'driverId');
  }
};
