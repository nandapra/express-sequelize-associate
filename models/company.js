'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Company extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Company.hasMany(
        models.Employee,
        {
          foreignKey: 'companyId',
          as: 'as_employees'
        }
      )

      models.Company.hasMany(
        models.Tool,
        {
          foreignKey: 'companyId',
          as: 'as_companyTools'
        }
      )
    }
  };
  Company.init({
    name: DataTypes.STRING,
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Company',
  });
  return Company;
};