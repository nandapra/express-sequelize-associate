'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Employee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Employee.belongsTo(
        models.Company,
        {
          foreignKey: 'companyId',
          as: 'as_companies'
        }
      )

      models.Employee.belongsToMany(
        models.Tool,
        {
          through: 'Pivot',
          foreignKey: 'employeeId',
          as: 'as_tool'
        }
      )

      models.Employee.belongsToMany(
        models.Driver,
        {
          through: 'Pivot',
          foreignKey: 'driverId',
          as: 'as_driver'
        }
      )
    }
  };
  Employee.init({
    name: DataTypes.STRING,
    phone: DataTypes.INTEGER,
    address: DataTypes.STRING,
    LOS: DataTypes.INTEGER,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Employee',
  });
  return Employee;
};