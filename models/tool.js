'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Tool extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Tool.belongsToMany(
        models.Employee,
        {
          through: 'Pivot',
          foreignKey: 'employeeId',
          as: 'as_employeeTool'
        }
      )

      models.Tool.belongsTo(
        models.Company,
        {
          foreignKey: 'companyId',
          as: 'as_toolCompany'
        }
      )
    }
  };
  Tool.init({
    name: DataTypes.STRING,
    category: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Tool',
  });
  return Tool;
};