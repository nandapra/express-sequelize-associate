# Docker (Latihan)
1. Satu Docker hanya bisa menjalankan satu images
2. Satu Images bisa dijalankan oleh banyak Docker

# Syntax Docker:
1. docker
2. docker images								==> {untuk melihat images yang ada}
3. docker ps									==> {untuk melihat container yang ada}
4. docker stop <<container id>>					==> {untuk menghapus container}
5. docker rmi <<images id>>						==> {untuk remove images}
6. docker exec -it  <<container id>> /bin/bash	==> {untuk masuk ke container}


# Build Project:
1. Buat file .dockerignore
2. Buat file Dockerfile
3. docker build -t <<nama project>> .				==> {untuk build docker images}
4. docker run -dp <port>>:<<port>> <<nama project>>	==> {running docker images}

# Noted:
PM2 untuk menjalankan nodejs kita agar tidak terputus