var express = require('express');
var router = express.Router();
var CompanyController = require ('../controllers/companyController')

const companyController = new CompanyController()

/* GET home page. */
router.get('/', companyController.list)

module.exports = router;
