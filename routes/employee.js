var express = require('express');
var router = express.Router();
var employeeController = require ('../controllers/employeeController')
var passport = require('passport')

/* GET home page. */
router.get(
    '/', 
    passport.authenticate('jwt', { session: false }), 
    employeeController.list
)

router.post('/', employeeController.create)

module.exports = router;
