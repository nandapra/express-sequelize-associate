var express = require('express');
var router = express.Router();
var toolController = require ('../controllers/toolController')

/* GET home page. */
router.get('/', toolController.list)

module.exports = router;
