'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Companies',
      [{
        id: 1,
        name: 'PT Telkomsel',
        address: 'Jl Gatot Subroto',
        createdAt: new Date,
        updatedAt: new Date
      }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(
      'Companies',
      null, {})
  }
};
