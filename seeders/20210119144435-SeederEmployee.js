'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Employees',
      [{
        id: 1,
        name: 'Nanda Cahyangtia Pratama',
        address: 'Jl Suparto No 2',
        phone: '628112348007',
        createdAt: new Date,
        updatedAt: new Date,
        LOS: 3,
        companyId: 1,
        password: ""
      }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(
      'Employees',
      null, {})
  }
};
