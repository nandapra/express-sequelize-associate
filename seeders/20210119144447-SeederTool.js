'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Tools',
      [{
        id: 1,
        name: 'INEoM',
        category: 'Monitoring Network',
        createdAt: new Date,
        updatedAt: new Date,
        companyId: 1
      }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(
      'Tools',
      null, {})
  }
};
