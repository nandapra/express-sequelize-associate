'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Pivots',
      [{
        id: 1,
        toolId: 1,
        employeeId:1,
        createdAt: new Date,
        updatedAt: new Date,
        driverId: 1
      }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(
      'Pivots',
      null, {})
  }
};
