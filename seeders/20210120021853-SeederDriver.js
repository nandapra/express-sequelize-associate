'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Drivers',
      [{
        id: 1,
        name: "Saeful Huda",
        location: "Jakarta",
        createdAt: new Date,
        updatedAt: new Date
      }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(
      'Drivers',
      null, {})
  }
};
